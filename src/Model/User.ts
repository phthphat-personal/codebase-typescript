import { DatabaseExcution } from './DatabaseExcution';

// const connection = createConnection()
export default class User extends DatabaseExcution {
    loadAllUsers(): Promise<any[]> {
        return new Promise( (resolve, reject) => {
            this.executeQuery(`select * from ${this.table.user}`, [])
                .then( value => {
                    resolve(value)
                })
                .catch( err => {
                    reject(err)
                })
        })
    }
}