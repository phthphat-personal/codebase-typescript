import { Connection, createConnection } from 'mysql'
//Config
import config from '../../config.json'
import { Table } from '../AppConstant'

export class DatabaseExcution {
    
    table: Table = new Table()

    public get connection() : Connection {
        return createConnection({
            host: config.database.host,
            user: config.database.username,
            password: config.database.password,
            port: config.database.port,
            database: config.database.schema
        })
    }

    public executeQuery(query: string, value: any): Promise<any[]> {
        return new Promise((resolve, reject) => {
            this.connection.connect()
            this.connection.query(query, value, (err, rows, _) => {
                if (err) reject(err)
                else {
                    resolve(rows)
                }
            })
            this.connection.end()
        })
    }
}