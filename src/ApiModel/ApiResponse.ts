export interface ApiData<T> {
    message: string
    data?: T
    code: number
    status: boolean
}

export default class ApiResponse {

    toError(message: string, code: number): ApiData<undefined> {
        let apiData: ApiData<undefined> = {
            message: message,
            code: code,
            status: false
        }
        return apiData
    }
    toData<T>(data: T, code: number = 200): ApiData<T> {
        let apiData: ApiData<T> = {
            message: "",
            code: code,
            data: data,
            status: true
        }
        return apiData
    }
}