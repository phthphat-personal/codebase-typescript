import express from 'express'
import ApiResponse from '../ApiModel/apiResponse'
//Class
import User from '../Model/User'

var router = express.Router()
var apiRes = new ApiResponse
const userModel = new User()

router.get('/', (req, res) => {
    userModel.loadAllUsers()
    .then(value => {
        res.send(apiRes.toData(value))
    })
    .catch( err => {
        res.send(apiRes.toError(err.message, 404))
    })
})

export default router