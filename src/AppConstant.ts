//Config
import config from '../config.json'


export class Table {   
    databaseName: string = config.database.schema
    public get user() : string {
        return this.databaseName + ".User"
    }
}