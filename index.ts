import express from 'express'
import bodyParser from 'body-parser'
//Route
import UserRoute from './src/Route/UserRoute'
//Config
import config from './config.json'

const app = express()

app.use(bodyParser.json())

app.use('/user', UserRoute)
app.get('/', (req, res) => {
    res.send({
        message: 'Welcome to server'
    })
})


let host = config.server.host
let port = config.server.port
app.listen(port, host, (err) => {
    console.log(err ? err : `Server is at ${host}:${port}`)
})